# API REST

## Setup

create .env

```bash
node -e "console.log('JWTSECRET=\"'+require('crypto').randomBytes(35).toString('hex')+'\"')" > .env
```

Run dev Enviroment

```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d --remove-orphan --force-recreate
```

Restore database

```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml exec -T db mysql -u root --password=pass_root < database/database.sql
```

Run prod Enviroment

```bash
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up --build -d --remove-orphan
```

test endpoints

```bash
#get version
curl http://localhost:3000/version --silent
```

```bash
#auth
curl --silent --location --request POST 'http://localhost:3000/auth' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username" : "admin",
    "password" : "abc@123"
}'
```

### down services

```bash
docker-compose -f docker-compose.yml -f docker-compose.dev.yml down
```
