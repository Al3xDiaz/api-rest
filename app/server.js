/*imports */
const express = require("express")
const jwt = require('jsonwebtoken')
const bcrypt = require("bcryptjs")
const {getVersion} = require('./services/configurations')
const bodyparser = require('body-parser');
const { getUser } = require('./services/users')
const cookieParser = require("cookie-parser");


const app = express()
app.use(bodyparser.json());
app.use(cookieParser());

const JWTSECRET = process.env.JWTSECRET.trim()
app.get('/',async(req,res)=>{
    return res.send("welcome to api 2.0")
})

app.get('/version',async(req,res)=>{
    try {
        const token = req.cookies.jwt
        const version =await getVersion()
        if (token) {
            jwt.verify(token, JWTSECRET, (err, decodedToken) => {
            if (err) {
                console.log(err);
                return res.status(401).json({ message: "Not authorized" })
            } else {
                return res.status(401).json({ message: "",data:{
                    version
                } })
            }
            })
        } else {
            return res
            .status(401)
            .json({ message: "Not authorized, token not available" })
        }       
    } catch (error) {
        console.log(error);
        return res.status(500).json({message:"opps, internal server error"})
    }
})

app.post('/auth',async (req,res)=>{
    try{
        const {username, password } = req.body
        const user =await getUser(username,password)
        if (!user){
            res.statusCode= 401
            res.send()            
        }
        
        const maxAge = 3 * 60 * 60;
        const token = jwt.sign(
            { id: user._id, username, role: user.role },
            JWTSECRET,
            {
                expiresIn: maxAge, // 3hrs in sec
            }
        );
        res.cookie("jwt", token, {
            httpOnly: true,
            maxAge: maxAge * 1000, // 3hrs in ms
        });
        res.status(200).json({
            message: "User successfully Logged in",
            user: username,
            token
        });
    }catch (error){
        console.log(error);
        return res.status(500).json({message:"opps, internal server error"})
    }
})

const port = process.env.PORT || 3000;
app.listen(port,() => console.log(`Listening on port ${port}...`))