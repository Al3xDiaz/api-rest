const { Select } = require("../conection")

const getVersion = async () => {
    const query = `SELECT version FROM configurations`;
    const rows = await Select(query);
    const {version}= rows[0]

    return version
}

module.exports = {
    getVersion
}