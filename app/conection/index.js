const mysql = require('mysql');


const {
    DB_HOST,
    DB_USER,
    DB_PASSWORD,
    DB_DATABASE
} = process.env


//MySQL details
var mysqlConnection = mysql.createConnection({
    host: DB_HOST.trim() || 'db',
    user: DB_USER.trim() || 'admin',
    password: DB_PASSWORD.trim() || 'pass',
    database: DB_DATABASE.trim() || 'test',
    multipleStatements: true
});
mysqlConnection.connect((err)=> {
    if(!err)
    console.log('Connection Established Successfully');
    else
    console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
});
const Select =(query)=>{
    return new Promise((resolve,reject)=>{
        mysqlConnection.query(query, (err, rows, fields) => {
            if (!err)
                resolve(rows)
            else
                reject(err)
            })
    })
}

module.exports = {
    Select,
}